# Updates bamboo.cfg.xml through Augeas
define bamboo::bamboo_cfg(
  String $value,
  Boolean $show_diff = true,
){
  $path = "application-configuration/properties/property[#attribute/name = '${name}']"

  augeas { "bamboo.cfg.xml:${name}":
    lens      => 'Xml.lns',
    incl      => "${bamboo::data_dir}/bamboo.cfg.xml",
    show_diff => $show_diff,
    changes   => [
      sprintf("set ${path}/#text %s", String($value, '%p')),
    ],
    onlyif    => "match ${path} size != 0",
  }
}
