# Setup Bamboo server systemd service
class bamboo::service {
  file { '/etc/systemd/system/bamboo.service':
    ensure  => file,
    content => epp('bamboo/bamboo.service'),
    owner   => 'root',
    group   => 'root',
    mode    => '0664',
  }
  -> service { 'bamboo':
    ensure   => running,
    enable   => true,
    provider => systemd,
  }
}
