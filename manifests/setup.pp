# Sets up Bamboo server
class bamboo::setup {
  bamboo_setup_license { 'current':
    ensure  => present,
    license => $bamboo::license,
  }

  bamboo_setup_config { 'current':
    ensure             => present,
    instance_name      => $bamboo::configuration['instance_name'],
    base_url           => $bamboo::configuration['base_url'],
    config_dir         => $bamboo::configuration['config_dir'],
    build_dir          => $bamboo::configuration['build_dir'],
    build_working_dir  => $bamboo::configuration['build_working_dir'],
    artifacts_dir      => $bamboo::configuration['artifacts_dir'],
    repository_log_dir => $bamboo::configuration['repository_log_dir'],
    broker_url         => $bamboo::configuration['broker_url'],
  }

  bamboo_setup_database { 'current':
    ensure    => present,
    type      => $bamboo::database['type'],
    driver    => $bamboo::database_driver,
    url       => $bamboo::database['url'],
    username  => $bamboo::database['username'],
    password  => $bamboo::database['password'],
    overwrite => $bamboo::database['overwrite'],
  }

  bamboo_setup_administrator { 'current':
    ensure    => present,
    username  => $bamboo::administrator['username'],
    password  => $bamboo::administrator['password'],
    full_name => $bamboo::administrator['full_name'],
    email     => $bamboo::administrator['email'],
  }
}
