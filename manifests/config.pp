# Configure Bamboo server
class bamboo::config {
  bamboo::bamboo_cfg {
    'bamboo.jms.broker.uri':
      value => $bamboo::configuration['broker_url'],
    ;
    'bamboo.jms.broker.client.uri':
      value => $bamboo::configuration['broker_client_url'],
    ;
    'hibernate.connection.driver_class':
      value => $bamboo::database_driver,
    ;
    'hibernate.connection.url':
      value => $bamboo::database['url'],
    ;
    'hibernate.connection.username':
      value => $bamboo::database['username'],
    ;
    'hibernate.connection.password':
      value => $bamboo::database['password'],
    ;
    'hibernate.dialect':
      value => $bamboo::database_dialect,
    ;
    'license.string':
      value     => $bamboo::license,
      show_diff => false, #Hide license details
    ;
  }
}
