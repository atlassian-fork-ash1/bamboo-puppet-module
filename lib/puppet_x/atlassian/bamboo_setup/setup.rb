require 'net/http'
require 'json'

module BambooSetup
  class Setup
    DEFAULT_BAMBOO_HOST = 'localhost'
    DEFAULT_BAMBOO_PORT = 8085

    def self.default
      @default ||= Setup.new()
    end

    def initialize(bamboo_host = DEFAULT_BAMBOO_HOST, bamboo_port = DEFAULT_BAMBOO_PORT)
      @bamboo_host = bamboo_host
      @bamboo_port = bamboo_port
      @retries = 10
      @timeout = 2
    end

    # Shows the current step in the setup process of a Bamboo instance.
    #
    # This is done by loading the main page and checking the URL of the page
    # we're being redirected to. If it is in the `/setup` domain the Bamboo server
    # hasn't been set up entirely yet. Otherwise, it's ready to be used.
    #
    # Can throw an exception if the `/setup` page we're redirected to is unknown to the system.
    def get_current_step()
      current_url = get_homepage_redirect()
      if current_url.start_with?('/setup/setupLicense.action')
        return Step::INITIAL
      elsif current_url.start_with?('/setup/setupGeneralConfiguration.action')
        return Step::SETUP_GENERAL_CONFIGURATION
      elsif current_url.start_with?('/setup/setupDatabase.action')
        return Step::SETUP_DATABASE
      elsif current_url.start_with?('/setup/setupSelectImport.action')
        return Step::SETUP_SELECT_IMPORT
      elsif current_url.start_with?('/setup/setupAdminUser.action')
        return Step::SETUP_ADMIN_USER
      elsif !current_url.start_with?('/setup/')
        return Step::COMPLETE
      else
        raise InvalidStepError, "Redirected to an unknown setup step #{current_url}"
      end
    end

    # Executes the License setup Step::
    #
    # Given a valid license, this will ensure that the license step in the Bamboo server
    # setup is executed.
    def setup_license(license)
      run_setup_step('/setup/validateLicense.action', { 'licenseString' => license }, Step::INITIAL)
      #TODO: Check that the execution is successful
      return
    end

    # Executes the general configuration setup Step::
    #
    # Given a general configuration, this will ensure that the general
    # configuration step in the Bamboo server is executed.
    def setup_general_configuration(configuration)
      data = {
        'instanceName'      => configuration[:instance_name],
        'baseUrl'           => configuration[:base_url],
        'configDir'         => configuration[:config_dir],
        'buildDir'          => configuration[:build_dir],
        'buildWorkingDir'   => configuration[:build_working_dir],
        'artifactsDir'      => configuration[:artifacts_dir],
        'repositoryLogsDir' => configuration[:repository_log_dir],
        'brokerUrl'         => configuration[:broker_url],
      }
      run_setup_step('/setup/validateGeneralConfiguration.action', data, Step::SETUP_GENERAL_CONFIGURATION)
      #TODO: Check that the execution is successful
      return
    end

    # Executes the database configuration setup Step::
    #
    # Given a database configuration, this will ensure that the database step in
    # the Bamboo server setup is executed.
    # It will wait for the database to be initialised.
    def setup_database_configuration(configuration)
      data = {
        'selectedDatabase'             => configuration[:type],
        'connectionChoice'             => 'jdbcConnection',
        'dbConfigInfo.driverClassName' => configuration[:driver],
        'dbConfigInfo.databaseUrl'     => configuration[:url],
        'dbConfigInfo.userName'        => configuration[:username],
        'dbConfigInfo.password'        => configuration[:password],
        'dataOverwrite'                => configuration[:overwrite],
      }
      path = '/setup/performSetupDatabaseConnection.action'
      run_setup_step(path, data, Step::SETUP_DATABASE)
      wait_for_setup(path)
      #TODO: Check that the execution is successful
      return
    end

    # Executes the import setup Step::
    #
    # This step is currently not supported and it's assumed that the installation is clean
    def setup_import()
      run_setup_step('/setup/performImportData.action', {'dataOption' => 'clean'}, Step::SETUP_SELECT_IMPORT)
      #TODO: Check that the execution is successful
      return
    end

    # Executes the admin setup Step::
    #
    # Given the admin configuration, this will ensure that the administrator step in
    # the Bamboo server set is executed.
    # It will wait for the instance to be completely initialised.
    def setup_administator(configuration)
      data = {
        'username'        => configuration[:username],
        'password'        => configuration[:password],
        'confirmPassword' => configuration[:password],
        'fullName'        => configuration[:full_name],
        'email'           => configuration[:email],
      }
      run_setup_step('/setup/performSetupAdminUser.action', data, Step::SETUP_ADMIN_USER)

      res2 = Net::HTTP.start(@bamboo_host, @bamboo_port) { |http|
        req = Net::HTTP::Get.new('/setup/finishsetup.action')
        # Required to avoid XSS alerts
        req['X-Atlassian-Token'] = 'no-check'
        req['Cookie'] = @cookies if @cookies

        http.request(req)
      }

      wait_for_setup('/setup/finishsetup.action')
      #TODO: Check that the execution is successful
      return
    end

    private

    # Waits for a setup step to be finished.
    #
    # As part of the Bamboo server setup, a database initialisation is started.
    # When the administrator is set up, the entire instance is initialised.
    def wait_for_setup(path)
      loop do
        res = post_configuration(path, {'statusRequest' => 'true'})

        break if JSON.parse(res.body)['completed']
        sleep 2
      end
      return
    end

    # Runs a setup step
    #
    # Runs a setup step given a path (URL) and data to submit.
    # The `expected_step` ensure that the setup is indeed waiting at the desired Step::
    def run_setup_step(path, data, expected_step)
      pre_step = get_current_step()
      raise IncorrectStepError, "Couldn't run the setup step, expected step #{expected_step}, got #{pre_step} instead" if pre_step != expected_step

      return post_configuration(path, data)
    end

    # Submits a bit of configuration to the Bamboo Server.
    def post_configuration(path, data)
      ensure_running()
      return Net::HTTP.start(@bamboo_host, @bamboo_port) { |http|
        req = Net::HTTP::Post.new(path)
        # Required to avoid XSS alerts
        req['X-Atlassian-Token'] = 'no-check'
        req['Cookie'] = @cookies if @cookies
        req.set_form_data(data)

        res = http.request(req)
        @cookies = res['set-cookie'] if res['set-cookie']
        return res
      }
    end

    # Finds the URL to which the homepage redirects to
    def get_homepage_redirect()
      default_path = '/'
      limit = 10
      ensure_running()
      return Net::HTTP.start(@bamboo_host, @bamboo_port) { |http|
        return get_last_redirect(http, default_path, limit)
      }
    end

    # Follows a chain of HTTP redirections and return the last element of the chain.
    def get_last_redirect(http, path, limit)
      #TODO: Come up with a better exception
      raise ArgumentError, 'too many HTTP redirects' if (limit < 0)

      res = http.get(path)
      if (Net::HTTPRedirection === res)
        return get_last_redirect(http, res['Location'], limit-1)
      else
        return path
      end
    end

    def ensure_running()
      for i in 1..@retries do
        begin
          Net::HTTP.start(@bamboo_host, @bamboo_port) { |http|
            result = http.get('/')
          }
          Puppet.debug("Connected")
          return
        rescue Errno::ECONNREFUSED, Errno::EADDRNOTAVAIL => e
          Puppet.debug("Waiting #{@timeout} seconds before trying again.")
          sleep(@timeout)
        end
      end
      fail("Bamboo service did not start up within #{@timeout * @retries} seconds. You should check the Bamboo log " +
        "files to see if something is wrong or consider increasing the timeout if the service is not starting up in " +
        "time.")
    end
  end

  class Step
    include Comparable

    attr :symbol
    attr :index
    @@symbols = {}

    def initialize(symbol, index)
      @symbol = symbol
      @index = index
      @@symbols[symbol] = self
    end

    def <=>(anOther)
      case anOther
      when Symbol
        index <=> self.class.from_symbol(anOther).index
      when Step
        index <=> anOther.index
      else
        raise 'Can\'t compare'
      end
    end

    def to_s
      "#{@symbol} (#{@index})"
    end

    def self.from_symbol(symbol)
      @@symbols[symbol]
    end

    INITIAL = Step.new(:initial, 0)
    SETUP_GENERAL_CONFIGURATION = Step.new(:setup_config, 1)
    SETUP_DATABASE = Step.new(:setup_database, 2)
    SETUP_SELECT_IMPORT = Step.new(:import, 3)
    SETUP_ADMIN_USER = Step.new(:setup_admin, 4)
    COMPLETE = Step.new(:complete, 5)
  end

  class BambooSetupError < StandardError
  end
  class InvalidStepError < BambooSetupError
  end
  class IncorrectStepError < BambooSetupError
  end
end
