require_relative '../bamboo_setup_step'
require_relative '../../../puppet_x/atlassian/bamboo_setup/setup'

Puppet::Type.type(:bamboo_setup_database).provide(:bamboo_setup_database, :parent => Puppet::Provider::BambooSetupStep) do
  def self.expected_step
    :setup_database
  end

  def create
    BambooSetup::Setup.default.setup_database_configuration(@resource)
    BambooSetup::Setup.default.setup_import()
  end

  def destroy
  end

  def exists?
    @property_hash[:ensure] == :present
  end

  mk_resource_methods
end
