require_relative '../../puppet_x/atlassian/bamboo_setup/setup'

class Puppet::Provider::BambooSetupStep < Puppet::Provider
  def self.instances
    current_step = BambooSetup::Setup.default.get_current_step
    [new({
      :name    => 'current',
      :ensure  => (current_step > expected_step())? :present : :absent,
    })]
  end

  def self.expected_step
    notice("Method 'expected_step' should be implemented")
  end

  def self.prefetch(resources)
    settings = instances
    resources.keys.each do |name|
      if provider = settings.find { |setting| setting.name == name }
        resources[name].provider = provider
      end
    end
  end
end
