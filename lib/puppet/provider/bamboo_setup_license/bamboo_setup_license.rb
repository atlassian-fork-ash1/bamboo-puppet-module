require_relative '../bamboo_setup_step'
require_relative '../../../puppet_x/atlassian/bamboo_setup/setup'

Puppet::Type.type(:bamboo_setup_license).provide(:bamboo_setup_license, :parent => Puppet::Provider::BambooSetupStep) do
  def self.expected_step
    :initial
  end

  def create
    BambooSetup::Setup.default.setup_license(@resource[:license])
  end

  def destroy
  end

  def exists?
    @property_hash[:ensure] == :present
  end

  mk_resource_methods
end
