require_relative '../bamboo_setup_step'
require_relative '../../../puppet_x/atlassian/bamboo_setup/setup'

Puppet::Type.type(:bamboo_setup_config).provide(:bamboo_setup_config, :parent => Puppet::Provider::BambooSetupStep) do
  def self.expected_step
    :setup_config
  end

  def create
    BambooSetup::Setup.default.setup_general_configuration(@resource)
  end

  def destroy
  end

  def exists?
    @property_hash[:ensure] == :present
  end

  mk_resource_methods
end
