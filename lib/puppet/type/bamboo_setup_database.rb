require 'puppet/property/boolean'

Puppet::Type.newtype(:bamboo_setup_database) do
  @doc = 'Sets up Bamboo server\'s database through the setup wizard.'
  ensurable

  newparam(:name) do
    desc 'Name of the setup (i.e. current).'
  end

  newparam(:type) do
    desc ''
  end

  newparam(:driver) do
    desc ''
  end

  newparam(:url) do
    desc ''
  end

  newparam(:username) do
    desc ''
  end

  newparam(:password) do
    desc ''
  end

  newparam(:overwrite) do
    desc ''
  end

  autorequire(:bamboo_setup_license) do
      self[:name]
  end

  autorequire(:bamboo_setup_config) do
      self[:name]
  end
end
