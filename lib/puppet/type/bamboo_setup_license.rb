require 'puppet/property/boolean'

Puppet::Type.newtype(:bamboo_setup_license) do
  @doc = 'Sets up Bamboo server\'s license through the setup wizard.'
  ensurable

  newparam(:name) do
    desc 'Name of the setup (i.e. current).'
  end

  newparam(:license) do
    desc 'Bamboo Server License'
  end
end
