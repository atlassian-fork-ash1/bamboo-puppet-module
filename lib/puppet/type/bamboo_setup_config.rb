require 'puppet/property/boolean'

Puppet::Type.newtype(:bamboo_setup_config) do
  @doc = 'Sets up Bamboo server\'s general configuration through the setup wizard.'
  ensurable

  newparam(:name) do
    desc 'Name of the setup (i.e. current).'
  end

  newparam(:instance_name) do
    desc 'Name of the Bamboo instance'
  end

  newparam(:base_url) do
    desc 'URL used to access the Bamboo instance'
  end

  newparam(:config_dir) do
    desc ''
  end

  newparam(:build_dir) do
    desc ''
  end

  newparam(:build_working_dir) do
    desc ''
  end

  newparam(:artifacts_dir) do
    desc ''
  end

  newparam(:repository_log_dir) do
    desc ''
  end

  newparam(:broker_url) do
    desc 'URL on which the ActiveMQ broker will listen'
  end

  autorequire(:bamboo_setup_license) do
      self[:name]
  end
end
